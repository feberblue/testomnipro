import NavBarApp from "../components/screan1/navbar";
import ProductImage from "../components/screan1/productimg";
import ProductHeader from '../components/screan1/productheader';
import ProductSpecs from '../components/screan1/specs';
import Footer from "../components/screan1/footer";
import Cantidad from '../components/screan1/cnt';
import BtnAdd from "../components/screan1/btnAdd";
import DescriptionProduct from "../components/screan1/description";
import LikesProducts from "../components/screan1/likes";

const ScreenWeb = () => {
  return (
    <>
      <NavBarApp></NavBarApp>
      <ProductImage></ProductImage>
      <ProductHeader 
        productName='Pantalones para Damas "The Sideswept Dhoit"'
        ratingProduct={4} 
        priceProduct={139.99} />
      <div className="section">
        <ProductSpecs></ProductSpecs>
        <Cantidad></Cantidad>
        <BtnAdd label="Añadir al carrito" classBtn="btnAddCar" classContainer="containerAddCar"></BtnAdd>
      </div>
      <DescriptionProduct
       title="Descripción"
       description="In eu blandit metus. Phasellus vitae consequat augue. Cras auctor lacus a purus convallis..."  />
       <LikesProducts></LikesProducts>
      <Footer></Footer>
    </>
  );
};

export default ScreenWeb;
