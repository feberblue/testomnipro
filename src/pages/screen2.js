import HeaderLogin from "../components/login/header";
import TabsLogin from "../components/login/tabs";
import arriba from '../images/arriba.svg';
import derecha from '../images/derecha.svg';
import izquierda from '../images/izquierda.svg';
import logoR from '../images/R.svg';

const ScreenLogin = () => {
    return <>
        <div className="contLogin">
            <img src={arriba} className="svgArriba"></img>
            <img src={derecha} className="svgDerecha"></img>
            <img src={izquierda} className="svgIzquierda"></img>
            <img src={logoR} className="svgR"></img>
            <HeaderLogin></HeaderLogin>            
            <TabsLogin />            
        </div>
    </>;
}

export default ScreenLogin;