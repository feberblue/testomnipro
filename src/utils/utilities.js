const currencyFormater = (value, styleName) => {
    const absValue = Math.abs(value);
    const nroDecimal = 2;

    const returnCurrency = new Intl.NumberFormat('en-US', {
        style: styleName,
        currency: 'USD',
        minimumFractionDigits: nroDecimal,
        maximumFractionDigits: nroDecimal
    }).format(absValue);

    return value < 0 ? `- $${returnCurrency}` : `$${returnCurrency}`;
}

const isNullOrEmptyString = (value) => {
    if (value === undefined || value === null || value === "")
        return true;
    return false;
}

export { currencyFormater, isNullOrEmptyString }