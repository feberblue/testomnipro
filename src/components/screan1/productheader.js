import { currencyFormater } from '../../utils/utilities';

export const TitleProductHeader = (props) => {
    const { title } = props;
    return <div className="titleProductHeader">{title}</div>;
}

export const RatingProduct = (props) => {
    const { value } = props;
    const listStart = [1, 2, 3, 4, 5];
    const listStartRed = listStart.map((number) => {
        return <i key={number} className="fas fa-star productStarts"></i>
    });


    return <div className="ratingProduct">
        {listStartRed}
        <span className="productStartOf">{`${value} de 5`}</span>
    </div>;
}

export const PriceProduct = (props) => {
    const { value } = props;
    const valueFormat = currencyFormater(value, 'decimal');
    return <div className="priceProduct">{valueFormat}</div>;
}

const ProductHeader = (props) => {
    const { productName, ratingProduct, priceProduct } = props;
    return <div className="productHeader">
        <TitleProductHeader title={productName}></TitleProductHeader>
        <RatingProduct value={ratingProduct}></RatingProduct>
        <PriceProduct value={priceProduct} />
    </div>;
};

export default ProductHeader;