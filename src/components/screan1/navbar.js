const NavBarApp = () => {
    return (
        <>
            <header>
                <div className="navBar">
                    <a href="#logo" className="logo nav-link">Logo</a>
                    <ul className="nav-menu">
                        <li className="nav-menu-item"><a href="#search" className="nav-menu-link nav-link"><i className="fas fa-search"></i></a></li>
                        <li className="nav-menu-item"><a href="#favorite" className="nav-menu-link nav-link"><i className="far fa-heart"></i></a></li>
                        <li className="nav-menu-item"><a href="#shop" className="nav-menu-link nav-link"><i className="fas fa-shopping-bag"></i></a></li>
                        <li className="nav-menu-item"><a href="#menuapp" className="nav-menu-link nav-link"><i className="fas fa-bars"></i></a></li>
                    </ul>
                </div>
            </header>
        </>
    );
};

export default NavBarApp;