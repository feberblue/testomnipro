

const DescriptionProduct = (props) => {
    const { title, description } = props;
    return <div className="cntDescription">
        <div className="titleDescription">{title}</div>
        <p className="txtDescription">{description}</p>
        <div className="readMore">Leer más</div>
    </div>;
}

export default DescriptionProduct;