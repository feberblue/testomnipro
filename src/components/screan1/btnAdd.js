
const BtnAdd = (props) => {
    const { label, classBtn, classContainer } = props;
    return <div className={classContainer}>
        <button className={classBtn}>{label}</button>
    </div>
};

export default BtnAdd;