import { useState } from "react";


const Cantidad = () => {
    const [contadorProducto, setContadorProducto] = useState(0);

    const increment = () => {
        let number = document.querySelector('[name="number"]');
        number.value = parseInt(number.value) + 1;
        setContadorProducto(number.value);
    }

    const decrement = () => {
        let number = document.querySelector('[name="number"]');
        if (parseInt(number.value) > 0) {
            number.value = parseInt(number.value) - 1;
            setContadorProducto(number.value);
        }
    }

    const changeText = (e) => {
        setContadorProducto(e.target.value);
    }

    return <div className="cntProduct">
        <div className="btnGroups">
            <span className="labelGroups">Cantidad</span>
            <div className="ContContador">
                <button onClick={decrement} className="btnDecrement">-</button>
                <input name="number" id="number" onChange={changeText} type="text" readOnly defaultValue={contadorProducto} className="inputIncrement" />
                <button onClick={increment} className="btnIncrement">+</button>
            </div>
        </div>

    </div>;
};

export default Cantidad;