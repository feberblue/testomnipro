import { currencyFormater } from "../../utils/utilities";

export const ListShowProducts = () => {
    return <>
        <div className="cntListProducts">
            <ShowProduct productName="Producto 1" price={130}></ShowProduct>
            <ShowProduct productName="Producto 2" price={130}></ShowProduct>
            <ShowProduct productName="Producto 3" price={130}></ShowProduct>
        </div>
    </>;
}

export const ShowProduct = (props) => {
    const {productName, price} = props;
    const valueFormat = currencyFormater(price, 'decimal');
    return <>
        <div className="cntShowProduct">
            <div className="cntShowImageProduct"></div>
            <div className="showNameProduct">{productName}</div>
            <div className="showPriceProduct">{valueFormat}</div>
        </div>

    </>;
}

const LikesProducts = () => {
    return <div className="cntLikeProduct">
        <div className="titleLike">También te podría gustar</div>
        <ListShowProducts />
    </div>
}
export default LikesProducts;