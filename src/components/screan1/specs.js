const ProductSpecs = () => {
    return <div className="productSpecs">
        <div className="productSize">
            <div className="btnGroups">
                <span className="labelGroups">Tamaño</span>
                <button className="btnSpecs" disabled>S</button>
                <button className="btnSpecs">M</button>
                <button className="btnSpecs btnSelected">L</button>
                <button className="btnSpecs" disabled>XL</button>
                <button className="btnSpecs" disabled>XXL</button>
            </div>
        </div>
        <div className="productkit">
            <div className="btnGroups">
                <span className="labelGroups">kit</span>
                <button className="btnkits btnSelected" >Home</button>
                <button className="btnkits">Away</button>
                <button className="btnkits ">Third</button>
            </div>
        </div>
    </div>;
};

export default ProductSpecs;