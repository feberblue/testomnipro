
const HeaderLogin = () => {
    return <header>
        <div className="navBarLogin">
        <a href="#logo" className="logo nav-link"></a>
            <ul className="nav-menu">
                <li className="nav-menu-item-login"><a href="#menuapp" className="nav-menu-link nav-link-login"><i className="fas fa-bars"></i></a></li>
            </ul>
        </div>
    </header>;
}

export default HeaderLogin;