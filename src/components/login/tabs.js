import TabLogin from "./login";
import TabRegister from "./register";

const TabsLogin = () => {
    return <>
        <div className="contentTabs">            
            <TabLogin></TabLogin>
            <TabRegister></TabRegister>
        </div>
    </>;
}

export default TabsLogin;