
const TabLogin = () => {
    return <>
        <form autoComplete="off">
            <div className="containerTab">
                <div className="groupControl">
                    <label className="labelControl" htmlFor="email">Email</label>
                    <input type="email" name="email" id="email" className="txtControl" placeholder="ejemplo@email.com" autoComplete="off"></input>
                </div>

                <div className="groupControl">
                    <label className="labelControl" htmlFor="pass">Contraseña</label>
                    <input type="password" name="pass" id="pass" className="txtControl" placeholder="********" autoComplete="off"></input>
                </div>

                <div className="centerContainerLogin">
                    <input type="checkbox" name="checkbox" className="cm-toggle" /> <span className="suscribe">Suscribirse al Newsletter</span>
                </div>

                <div className="centerContainerLogin">
                    <button className="btnLogin">Ingreso</button>
                </div>

                <div className="centerContainerLogin">
                    <a href="#olvidar" className="linkOlvidar">Olvide Contraseña</a>
                </div>
            </div>
        </form>

    </>;
}

export default TabLogin;