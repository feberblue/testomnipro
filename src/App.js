import { BrowserRouter as Router, Route } from 'react-router-dom';
import ScreenWeb from './pages/screan1';
import ScreenLogin from './pages/screen2';

function App() {
  return (
    <Router>
      <Route exact path="/" component={ScreenWeb}></Route>
      <Route path="/login" component={ScreenLogin}></Route>      
    </Router>
    
  );
}

export default App;
